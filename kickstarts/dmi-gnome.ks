# dmi-gnome.ks
#
# Main kickstart for GNOME.

%include fedora-live-workstation.ks
%include mixins/desktop-gnome.ks

# main localization
%include mixins/l10n/sr_RS-gnome.ks

# other supported languages

# features
%include mixins/nonfree.ks

%include mixins/development-gnome.ks
%include mixins/teams.ks

part / --size 9216 --fstype ext4
