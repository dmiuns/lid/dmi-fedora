# development.ks
#
# Development support base

repo --name=vscode --baseurl=https://packages.microsoft.com/yumrepos/vscode
repo --name=dusan --baseurl=https://repos.dusansimic.me/rpms

%packages

@development-tools
# editors
code
vim-enhanced
geany
eclipse

# java
java-11-openjdk-devel
java-latest-openjdk-devel
openjfx

# scala
scala

# haskell
ghc
cabal-install

# gcc
gcc

# libraries
svetovid-lib

# other
Qt4LibsForBouml
bouml

%end

%post

# svetovid classpath
cat > /etc/profile.d/svetovid-classpath.sh << EOF
# /etc/profile.d/svetovid-classpath.sh - exports classpath for svetovid library
export CLASSPATH=${CLASSPATH}:/usr/share/java/svetovid-lib.jar
EOF

%end
