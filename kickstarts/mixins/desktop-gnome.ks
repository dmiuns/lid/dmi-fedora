# desktop-gnome.ks
#
# Customization for GNOME desktop.

%include desktop-base.ks

%packages

# desktop
gnome-extensions-app
gnome-shell-extension-appindicator
gnome-tweaks

# networking
firewall-config
transmission-gtk

%end

%post

# enable drive menu, places menu, apps menu and app indicator
cat > /etc/dconf/db/local.d/01-pmf-gnome-extensions << EOF_EXTENSIONS
# global gnome extensions

[org/gnome/shell]
enabled-extensions=['drive-menu@gnome-shell-extensions.gcampax.github.com', 'places-menu@gnome-shell-extensions.gcampax.github.com', 'apps-menu@gnome-shell-extensions.gcampax.github.com', 'appindicatorsupport@rgcjonas.gmail.com']

EOF_EXTENSIONS

# enable minimize and maximize buttons
cat > /etc/dconf/db/local.d/01-pmf-gnome-window-buttons << EOF_WINBUTTONS
# global window buttons

[org/gnome/desktop/wm/preferences]
button-layout='appmenu:minimize,maximize,close'
EOF_WINBUTTONS

# update configuration
dbus-launch --exit-with-session dconf update

%end
