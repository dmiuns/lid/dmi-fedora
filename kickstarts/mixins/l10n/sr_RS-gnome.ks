# provides serbian localization for the GNOME desktop

%include sr_RS-base.ks

%post

gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'us'), ('xkb', 'rs+latin'), ('xkb', 'rs+alternatequotes')]"

%end
