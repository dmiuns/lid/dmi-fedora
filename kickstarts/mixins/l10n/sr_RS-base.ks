# sr_RS-base.ks
#
# Add Serbian keyboard input

%include sr_RS-support.ks

keyboard --xlayouts=us,'rs (latin)','rs (alternatequotes)'
timezone Europe/Belgrade
