# desktop-base.ks
#
# Common customizations for a desktop workstation

%packages

# unwanted stuff
-abrt*
-fedora-release-notes
-fpaste
-rsyslog
-sendmail

# multimedia
mozilla-openh264
vdpauinfo
libva-vdpau-driver
libva-utils

# fonts
google-noto-sans-fonts
google-noto-sans-mono-fonts
google-noto-serif-fonts
liberation-s*-fonts
wine-fonts

# tools
@networkmanager-submodules
htop
vim-enhanced
exfat-utils

%end

%post

# link wine fonts to system directory
ln -s /usr/share/wine/fonts /usr/share/fonts/wine

cat > /usr/local/sbin/firstboot << FIRSTBOOT_EOF
#!/bin/bash

extcode=0

shopt -s nullglob
for src in /usr/local/sbin/firstboot_*.sh; do
    echo "firstboot: running $src"
    $src
    if [ $? -ne 0 ]; then
        mv $src $src.failed
        echo "Script failed! Saved as: $src.failed"
        extcode=1
    else
        echo "Script completed"
        rm $src
    fi
done

if [[ $exitcode == 0 ]]; then
    semanage fcontext -a -t unconfined_exec_t '/usr/local/sbin/firstboot'
    rm /usr/local/sbin/firstboot
fi

exit $extcode
FIRSTBOOT_EOF

chmod +x /usr/local/sbin/firstboot

cat > /usr/local/sbin/firstboot_anaconda.sh << ANACONDA_EOF
#!/bin/bash
dnf remove -y anaconda
ANACONDA_EOF

chmod +x /usr/local/sbin/firstboot_anaconda.sh

%end
