# Fedora Remix for Department of mathematics and informatics

## Purpose

This repository contains the build script for a Fedora Remix that aims to offer a complete Fedora Linux desktop, fully ready for use in programming and computing related courses on Department of mathematics and informatics, Faculty of Sciences, University of Novi Sad.

We have taken care of:
* multimedia support
* tools and software selection
* desktop tweaks and extensions
* network configuration for campus networks

It's quite easy to toggle features by including or excluding "mixin" files.

You can build a LiveCD and try the software, and then install it on your PC if you want.

## Why Fedora?

Fedora is a feature-rich operating system which offers a complete suite of sofware for many purposes. All of the software needed in courses on DMI is available in official Fedora repositories or is fairly easy to install. Also Fedora is very easy to maintain and custom variants for.

## Installed software

Compilers and development libraries:
* OpenJDK 11 and latest
* OpenJFX 11
* Svetovid Lib
* GHC (Haskell)
* Scala
* gcc

Editors and IDEs:
* Geany
* Eclipse
* Visual Studio Code

Other software and tools:
* BOUML
* Microsoft Teams
* Some Gnome tweaks

Network configuration:
* PMF VPN and WiFi
* eduroam WiFi

## How to build the LiveCD

A Fedora system matching the target release version is required to build the images. The build system support using a podman container, so that another Linux host can be used.

Required dependencies are: `podman`, `qemu-kvm`, `make`. You will need root privileges for most things.

This usually means:

```sh
dnf install podman qemu-kvm make
```

To build, selinux must be set off. This means either disabling at runtime:

```sh
setenforce 0
```

Or it might require to (since Fedora 34+) adding a kernel option:

```
enforcing=0
```

GNU make is used to control the build process. For example:

Prepare the podman image used to build:

```sh
make podman-builder
```

Start clean:

```sh
make clean
```

Build the ISO files:

```sh
make
```

Test the live system in a virtual machine:

```sh
make test
```

Write the result to a USB drive:

```sh
make DEVICE=/dev/sdX disk-efi # or "disk-bios" for legacy BIOS mode
```

Clean up the build machine completely:

```sh
make clean podman-clean
```

By default podman is used to run the build steps but the make target can also be run directly:

```sh
make USE_PODMAN=no images
```

## Post install task

### firstboot script

After completing an installation a `firstboot` command can be executed to apply a few customizations
and cleanups that cannot be built into the installer.

```sh
firstboot
```

This will remove Anaconda and enable flathub.

## Change log

All notable changes to this project will be documented in the [CHANGELOG.md](./CHANGELOG.md) file.

## Based on [Fedora Remix](https://github.com/tierratelematics/fedora-remix)

This set of kickstarts for Fedora is based on
[Fedora Remix](https://github.com/tierratelematics/fedora-remix). Makefile is practically copied and
the main kickstart file and mixins are based on Fedora Remix's.

## Author

[Dušan Simić](https://dusansimic.me) <<dusan.simic1810@gmail.com>>
